# Bol.com Plaza Api V2

Installation:
```bash
$ composer require bol/bol-plaza-v2
```

**Note:** Bol.com requires you to use their parameter values:
* [Transporters](https://developers.bol.com/documentatie/plaza-api/appendix-a-transporters/)
* [Conditions](https://developers.bol.com/documentatie/plaza-api/appendix-b-conditions/)
* [Delivery codes](https://developers.bol.com/documentatie/plaza-api/appendix-c-delivery-codes/)
* [Reasons](https://developers.bol.com/documentatie/plaza-api/appendix-d-reasons-errors/)

### Client
```php
require_once 'vendor/autoload.php';

$publicKey = '<publicKey>';
$privateKey = '<privateKey>';
$test = true;

// For live enviroment, set the 3rd parameter to false or remove it
$client = new BOL\PlazaClient($publicKey, $privateKey, $test);
```