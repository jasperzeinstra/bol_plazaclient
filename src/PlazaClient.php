<?php
namespace BOL;

use Exception;
use DOMDocument;
use League\Csv\Reader;
use Mage;

class PlazaClient
{
    const CONTENT_TYPE = 'application/xml';

    protected $publicKey;
    protected $privateKey;
    protected $url;

    public $endPoints = [
        'orders' => '/services/rest/orders/v2',
        'offers-export' => '/offers/v2/export',
        'offer-delete' => '/offers/v2',
        'offer-update' => '/offers/v2'
    ];

    /**
     * Avalilable delivery codes
     * @var array $deliveryCodes
     */
    public $deliveryCodes = [
        '24uurs-23',
        '24uurs-22',
        '24uurs-21',
        '24uurs-20',
        '24uurs-19',
        '24uurs-18',
        '24uurs-17',
        '24uurs-16',
        '24uurs-15',
        '24uurs-14',
        '24uurs-13',
        '24uurs-12',
        '1-2d',
        '2-3d',
        '3-5d',
        '4-8d',
        '1-8d'
    ];

    /**
     * Construct the client
     * @param string $publicKey
     * @param string $privateKey
     * @param boolean $test
     * @throws Exception
     */
    public function __construct($publicKey, $privateKey, $test = false)
    {
        if (!$publicKey or !$privateKey) {
            throw new Exception('Either `$publicKey` or `$privateKey` not set');
        } else {
            $this->publicKey = $publicKey;
            $this->privateKey = $privateKey;
            if ($test) {
                $this->url = 'https://test-plazaapi.bol.com';
            } else {
                $this->url = 'https://plazaapi.bol.com';
            }
        }
    }

    /**
     * Convert an object to an array
     * @param  mixed $mixed
     * @return array
     */
    private function toArray($mixed)
    {
        return json_decode(
            json_encode($mixed),
            true);
    }

    /**
     * Determine if an array is associative
     * @param array $array
     * @return boolean
     */
    private function is_assoc(array $array)
    {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

    /**
     * Calculate the authorisation header
     * @param  string $httpMethod
     * @param  string $endPoint
     * @param  string $date
     * @return string
     */
    private function signature($httpMethod, $endPoint, $date)
    {
        $newline = "\n";
        $signature_string = $httpMethod . $newline . $newline;
        $signature_string .= self::CONTENT_TYPE . $newline;
        $signature_string .= $date . $newline;
        $signature_string .= "x-bol-date:" . $date . $newline;
        $signature_string .= preg_replace('/\?.*/', '', $endPoint);
        return $this->publicKey . ':' . base64_encode(
                hash_hmac('SHA256', $signature_string, $this->privateKey, true)
            );
    }

    /**
     * Do request to the Bol.com Plaza Api
     * @param string $endPoint
     * @param string $httpMethod
     * @param string $body
     * @return array|string
     * @throws Exception
     */
    public function request($endPoint, $httpMethod, $body = null)
    {
        $date = gmdate('D, d M Y H:i:s T');
        $httpMethod = strtoupper($httpMethod);
        $config = [
            CURLOPT_URL => $this->url . $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $httpMethod,
            CURLOPT_HTTPHEADER => array(
                "content-type: " . self::CONTENT_TYPE,
                "x-bol-authorization: " . $this->signature($httpMethod, $endPoint, $date),
                "x-bol-date: " . $date
            )
        ];

        if ($httpMethod !== 'GET' && !is_null($body)) {
            $config[CURLOPT_POSTFIELDS] = $body;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $config);
        $result = curl_exec($ch);

        if ($result === false) {
            throw new Exception(curl_error($ch));
        }

        $info = curl_getinfo($ch);

        if (strpos($info['content_type'], 'xml') !== false) {
            $result = simplexml_load_string(strtr($result, [
                'bns:1' => '',
                'bns:' => '',
                'ns1:' => ''
            ]));
        }

        if (isset($result->ServiceErrors->ServiceError)) {
            Mage::log(implode("\t", [$this->publicKey, $endPoint, $httpMethod, $body, $result->ServiceErrors->ServiceError->ErrorMessage]), null, 'request-error-'.$this->publicKey.'.log');
            throw new Exception($result->ServiceErrors->ServiceError->ErrorMessage);
        } else if ((int)substr($info['http_code'], 0, 1) !== 2) {
            Mage::log(implode("\t", [$this->publicKey, $endPoint, $httpMethod, $body, $info['http_code']]), null, 'request-error-'.$this->publicKey.'.log');
            throw new Exception($info['http_code']);
        }

Mage::log(implode("\t", [$this->publicKey, $endPoint, $httpMethod, $body]), null, 'request-'.Mage::helper('mct_bol/config')->getSellerId().'.log');

        if (is_object($result)) {
            return $this->toArray($result);
        } else if ($result !== '') {
            return $result;
        }

        return true;
    }

    /**
     * Request an offer file
     * @return string on success
     */
    public function requestOfferFile()
    {
        $result = $this->request($this->endPoints['offers-export'], 'GET');

        if (isset($result['Url'])) {
            $file = explode('/', $result['Url']);
            return end($file);
        }

        return false;
    }

    /**
     * Get the content from an offer file
     * @return array
     * @throws Exception
     */
    public function getOffers()
    {
        $fileName = $this->requestOfferFile();
        $offers = [];

        try {
            $csv = $this->request($this->endPoints['offers-export'] . '/' . $fileName, 'GET');
            $csv = Reader::createFromString($csv);
            $headers = $csv->fetchOne();
            foreach ($csv->setOffset(1)->fetchAll() as $row) {
                $offer = array_combine($headers, $row);
                $offer['Stock'] = (int)$offer['Stock'];
                $offer['Price'] = (float)$offer['Price'];
                $offer['Publish'] = $offer['Publish'] === 'TRUE' ? true : false;
                $offer['Published'] = $offer['Published'] === 'TRUE' ? true : false;
                $offers[] = $offer;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $offers;
    }

    /**
     * Geta single offer by EAN
     * @param $ean
     * @return array|string
     */
    public function getOffer($ean)
    {
        return $this->request('/offers/v2/' . $ean, 'GET');
    }

    /**
     * Delete an offer
     * @param string $ean
     * @param string $condition
     * @return bool
     */
    public function deleteOffer($ean, $condition = 'NEW')
    {
        $xml = new DOMDocument('1.0', 'UTF-8');
        $container = $xml->createElementNS('https://plazaapi.bol.com/offers/xsd/api-2.0.xsd', 'DeleteBulkRequest');
        $wrapper = $xml->createElement('RetailerOfferIdentifier');
        $ean = $xml->createElement('EAN', $ean);
        $condition = $xml->createElement('Condition', $condition);

        $xml->appendChild($container);
        $container->appendChild($wrapper);
        $wrapper->appendChild($ean);
        $wrapper->appendChild($condition);

        $this->request(
            $this->endPoints['offer-delete'],
            'DELETE',
            $xml->saveXML()
        );

        return true;
    }

    /**
     * Submit a new offer
     * @param  array $data
     * @return boolean
     * @throws Exception
     */
    public function createOffer(array $data)
    {
        return $this->updateOffer($data);
    }

    /**
     * Update an offer
     *
     * EAN
     * Condition
     * Price
     * DeliveryCode
     * QuantityInStock
     * Publish
     * ReferenceCode
     * Description
     * Title
     * FulfillmentMethod
     *
     * @param  array $data
     * @return boolean
     * @throws Exception
     */
    public function updateOffer(array $data)
    {
        if (isset($data['Publish'])) {
            $data['Publish'] = (bool)$data['Publish'] == true ? 'true' : 'false';
        }

        if (isset($data['Description'])) {
            $data['Description'] = htmlspecialchars($data['Description']);
        }

        if (isset($data['ReferenceCode'])) {
            $data['ReferenceCode'] = htmlspecialchars($data['ReferenceCode']);
        }

        if (isset($data['QuantityInStock']) && $data['QuantityInStock'] > 500) {
            $data['QuantityInStock'] = 500;
        }

        $xml = new DOMDocument('1.0', 'UTF-8');
        $container = $xml->createElementNS('https://plazaapi.bol.com/offers/xsd/api-2.0.xsd', 'UpsertRequest');
        $wrapper = $xml->createElement('RetailerOffer');
        $xml->appendChild($container);
        $container->appendChild($wrapper);

        foreach ($data as $key => $value) {
            $property = $xml->createElement($key, $value);
            $wrapper->appendChild($property);
        }

        return $this->request(
            $this->endPoints['offer-update'],
            'PUT',
            $xml->saveXML()
        );
    }

    /**
     * Get all orders
     * @return array
     */
    public function getOrders()
    {
        $result = $this->request(
            $this->endPoints['orders'],
            'GET'
        );

        if (isset($result['Order'])) {
            if (!isset($result['Order'][0]) && count($result['Order'])) {
                $result['Order'] = [$result['Order']];
            }
            foreach ($result['Order'] as $key => $order) {
                if (!isset($order['OrderItems']['OrderItem'][0]) && count($order['OrderItems']['OrderItem'])) {
                    $result['Order'][$key]['OrderItems']['OrderItem'] = [$order['OrderItems']['OrderItem']];
                }
            }

            return $result['Order'];
        }

        return [];
    }
}
